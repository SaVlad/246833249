#include "settings_window.h"
#include <tchar.h>

/// Supplimentary structure for callback data
typedef struct {
	settings_window_click_callback callback;
	LPARAM lParam;
} WIN_USERDATA;

/// Main window procedure
LRESULT CALLBACK wnd_proc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	if (uMsg == WM_CREATE) {
		LPCREATESTRUCT create = lParam; // Extracting creation parameters
		SetWindowLongPtr(hWnd, GWLP_USERDATA, create->lpCreateParams); // Storing parameter to userdata
	}
	if (uMsg == WM_COMMAND && HIWORD(wParam) == BN_CLICKED) { // Button clicked
		WIN_USERDATA*ud = GetWindowLongPtr(hWnd, GWLP_USERDATA); // Extracting callback from userdata
		if (ud && ud->callback) // Check if its set
			ud->callback(hWnd, lParam, ud->lParam); // Call callback
		return 0;
	}
	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

/// Creates window of size with callback
HWND create_window(settings_window_click_callback callback, LPARAM lParam, int w, int h) {
	HINSTANCE hInstance = GetWindowLongPtr(GetConsoleWindow(), GWLP_HINSTANCE); // Extract hInstance from console window
	LPCTSTR class_name = _T("class_name");
	WNDCLASS class; // Create default window class
	class.style = CS_HREDRAW | CS_VREDRAW;
	class.lpfnWndProc = wnd_proc;
	class.cbClsExtra = 0;
	class.cbWndExtra = 0;
	class.hInstance = hInstance;
	class.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_APPLICATION));
	class.hCursor = LoadCursor(NULL, IDC_ARROW);
	class.hbrBackground = COLOR_WINDOW + 1;
	class.lpszMenuName = NULL;
	class.lpszClassName = class_name;
	RegisterClass(&class); // Register window class
	WIN_USERDATA*ud = malloc(sizeof(WIN_USERDATA)); // Create callback data struct
	ud->callback = callback;
	ud->lParam = lParam;
	HWND hWnd = CreateWindow( // Create window
		class_name,
		_T(""),
		WS_SYSMENU | WS_CAPTION | WS_VISIBLE,
		CW_USEDEFAULT, CW_USEDEFAULT,
		w, h,
		NULL, NULL,
		hInstance, ud
	);
	return hWnd; // Return its handle
}
/// Adds button control to window
HWND add_button(HWND hWnd, int x, int y, int w, int h, LPCTSTR text) {
	return CreateWindow( // Create button control and set as window child
		_T("Button"), text,
		WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
		x, y, w, h, hWnd, NULL,
		GetWindowLongPtr(hWnd, GWLP_HINSTANCE), NULL
	);
}
/// Adds text control to window
HWND add_text(HWND hWnd, int x, int y, int w, int h, LPCTSTR text) {
	return CreateWindow( // Create text control and set as window child
		_T("Static"), text,
		WS_VISIBLE | WS_CHILD | SS_CENTER,
		x, y, w, h, hWnd, NULL,
		GetWindowLongPtr(hWnd, GWLP_HINSTANCE), NULL
	);
}