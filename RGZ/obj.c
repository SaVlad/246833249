#include "obj.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

/// Reads next float in string
/// Returns amount of characters read and stores result to v
int read_float(const char*str, float*v) {
	const char*end;
	const char*orig = str; // Remember original position

	// Skip all characters until -, +, ., null, new-line or digit is met
	for (; *str != '-' && *str != '+' && *str != '.' && *str != '\0' && *str != '\n' && (*str < '0' || *str > '9'); ++str);
	if (*str == '\0' || *str == '\n') // if null or new-line met
		return -1; // No number has been read
	*v = strtof(str, &end); // Read float
	return end - orig; // Return difference between current position and original
}
/// Reads next int in string
/// Returns amount of characters read and stores result to v
int read_int(const char*str, int*v) {
	const char*end;
	const char*orig = str; // Remember original position

	// Skip all characters until -, +, null, new-line or digit is met
	for (; *str != '-' && *str != '+' && *str != '\0' && *str != '\n' && (*str < '0' || *str > '9'); ++str);
	if (*str == '\0' || *str == '\n') // if null or new-line met
		return -1; // No number has been read
	*v = strtoll(str, &end, 10); // Read int
	return end - orig; // Return difference between current position and original
}
/// Reads next .obj face vertex in string
/// Returns amount of characters read and stores result to v
int read_face_vert(const char*str, int*v) {
	const char*orig = str; // Remember original position
	int r = read_int(str, v); // First int is actual vertex index
	if (r == -1) // No int has been read = no face vertex
		return -1;
	str += r; // Advance string after first int
	// Skip rest
	for (; *str == '/' || (*str >= '0' && *str <= '9'); ++str);
	return str - orig; // Return difference between current position and original
}
/// Reads .obj model from file
/// Returns error code and stores result in pObj
int read_obj(const char*path, obj*pObj) {
	FILE*file;
	int err;
	obj o = { 0, 0, 0, 0 }; // Initialize empty model
	size_t size;
	char*contents;
	int eol = 0;

	err = fopen_s(&file, path, "r"); // Try open file for read
	if (err)
		return err;
	fseek(file, 0, SEEK_END); // Get file size
	size = ftell(file);
	fseek(file, 0, SEEK_SET); // Reset pointer to beginning
	contents = calloc(size + 1, 1); // Allocate buffer big enough
	fread(contents, 1, size, file); // Read file contents to it
	fclose(file); // Close .obj file

	// Analyze each character
	for (int i = 0; i < size; ++i) {
		if (eol) { // We're in 'skip line' mode
			if (contents[i] == '\n') // If new-line is met
				eol = 0; // Exit eol mode
			continue; // Skip all characters until then
		}
		// If vertex declaration is met
		if (contents[i] == 'v' && contents[i + 1] == ' ') {
			o.vc++; // Increase vertex amount
			eol = 1;
		}
		// If face declaration is met
		if (contents[i] == 'f' && contents[i + 1] == ' ') {
			o.fc++; // Increase face amount
			eol = 1;
		}
	}
	// Allocate arrays
	o.f = malloc(sizeof(obj_face)*o.fc);
	o.v = malloc(sizeof(obj_vertex)*o.vc);
	o.fc = 0; // Reset sizes to use as counters
	o.vc = 0;
	eol = 0;
	// Iterate through string again
	for (int i = 0; i < size; ++i) {
		if (eol) {
			if (contents[i] == '\n')
				eol = 0;
			continue;
		}
		// Vertex declaration is met
		if (contents[i] == 'v' && contents[i + 1] == ' ') {
			i += 2; // Skip declaration
			i += read_float(&contents[i], &o.v[o.vc].x); // Read three floats
			i += read_float(&contents[i], &o.v[o.vc].y);
			i += read_float(&contents[i], &o.v[o.vc].z);
			--i; // Come back one character so we won't skip new-line
			++o.vc; // Increase vertex counter
			eol = 1; // Skip rest of the line
		}
		// Face declaration is met
		else if (contents[i] == 'f' && contents[i + 1] == ' ') {
			i += 2; // Skip declaration
			int orig = i; // Remember position
			int off;
			int t;
			o.f[o.fc].count = 0;
			// Count amount of vertices in face
			do {
				// Read face vertex
				off = read_face_vert(&contents[i], &t);
				if (off != -1) // If face vertex found
					o.f[o.fc].count++; // Increase counter
				i += off;
			} while (off != -1); // Until no face vertex is found
			i = orig; // Reset string position
			// Allocate vertices array
			o.f[o.fc].v = malloc(sizeof(obj_vertex)*o.f[o.fc].count);
			o.f[o.fc].count = 0;
			// Read face vertices
			do {
				// Read face vertex
				off = read_face_vert(&contents[i], &t);
				if (off != -1) // If face vertex found
					// Store vertex index
					o.f[o.fc].v[o.f[o.fc].count++] = t-1;
				i += off;
			} while (off != -1); // Until no face vertex is found
			++o.fc; // Increase face count
			eol = 1; // Skip rest of line
		}
	}
	*pObj = o;
	return 0;
}
/// Frees .obj model memory
void free_obj(obj o) {
	for (int i = 0; i < o.fc; ++i) // For each face
		free(o.f[i].v); // Free its vertex indices array
	free(o.f); // Free faces array
	free(o.v); // Free vertices array
}
/// Clone .obj model
obj obj_clone(obj o) {
	obj r;
	r.fc = o.fc; // Copy sizes
	r.vc = o.vc;
	r.f = malloc(sizeof(obj_face)*r.fc); // Allocate enough memory
	r.v = malloc(sizeof(obj_vertex)*r.vc);
	for (int i = 0; i < r.fc; ++i) {
		r.f[i].count = o.f[i].count; // Copy face indices count
		r.f[i].v = malloc(sizeof(int)*r.f[i].count); // Allocate memory for indices
		memcpy(r.f[i].v, o.f[i].v, sizeof(int) * r.f[i].count); // Copy indices
	}
	memcpy(r.v, o.v, sizeof(obj_vertex) * r.vc); // Copy vertices
	return r;
}
/// Determine if model needs triangulation
int obj_need_triangulation(obj o) {
	for (int i = 0; i < o.fc; ++i)
		if (o.f[i].count != 3)
			return 1;
	return 0;
}
/// Triangulate model faces
obj obj_triangulate(obj o) {
	obj r;
	r.vc = o.vc;
	r.v = malloc(sizeof(obj_vertex)*r.vc); // Allocate and copy vertices
	memcpy(r.v, o.v, sizeof(obj_vertex)*r.vc);
	r.fc = o.fc;
	for (int i = 0; i < o.fc; ++i)
		r.fc += o.f[i].count - 3; // This should add enough for all triangle faces
	r.f = malloc(sizeof(obj_face)*r.fc); // Allocate faces array
	r.fc = 0; // Reset amount to use as counter
	for (int i = 0; i < o.fc; ++i) { // Fill faces array
		int c = o.f[i].count; // Get face vertices amount
		if (c == 3) { // If it's already triangle
			r.f[r.fc++] = o.f[i]; // No action needed. Just copy face
			r.f[r.fc].count = c;
			r.f[r.fc].v = malloc(sizeof(obj_vertex)*c);
			memcpy(r.f[r.fc].v, o.f[i].v, sizeof(obj_vertex)*c);
		}
		else { // Triangulate face
			int v = 1; // Triangulation offset vertex
			while (c >= 3) { // Until face is triangle
				obj_face f; // Create new triangle face
				f.v = malloc(sizeof(int) * 3);
				f.count = 3;
				f.v[0] = o.f[i].v[0]; // Connect all vertices with zero index
				f.v[1] = o.f[i].v[v];
				f.v[2] = o.f[i].v[v + 1];
				r.f[r.fc++] = f; // Store new face
				v++; // Increase offset
				--c; // Decrease vertex amount
			}
		}
	}
	return r;
}