#pragma once
#ifndef OBJ_H
#define OBJ_H
#include <GL/glew.h>

/// .obj model vertex
typedef struct {
	float x, y, z; // Cooridnates
} obj_vertex;
/// .obj model face
typedef struct {
	int count; // Amount of vertices
	int*v;     // Vertex indices array
} obj_face;
/// .obj model
typedef struct {
	int vc;       // Vertex amount
	obj_vertex*v; // Vertex array
	int fc;       // Amount of faces
	obj_face*f;   // Face array
} obj;

/// Read .obj model from file
/// Return error. Model stored in pObj
int read_obj(const char*path, obj*pObj);
/// Frees .obj model memory
void free_obj(obj o);
/// Clones .obj model
obj obj_clone(obj o);
/// Determines if .model is not triangulated
int obj_need_triangulation(obj o);
/// Reduces all .obj model's faces to triangles
obj obj_triangulate(obj o);

#endif