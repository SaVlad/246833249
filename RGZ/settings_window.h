#pragma once
#ifndef SETTINGS_WINDOW_H
#define SETTINGS_WINDOW_H
#include <Windows.h>

/// Button click callback
typedef void (CALLBACK*settings_window_click_callback)(HWND hWnd, HWND hControl, LPARAM lParam);
/// Creates window of size with callback
HWND create_window(settings_window_click_callback callback, LPARAM lParam, int w, int h);
/// Adds button control to window and returns its handler
HWND add_button(HWND hWnd, int x, int y, int w, int h, LPCTSTR text);
/// Adds text control to window and returns its handler
HWND add_text(HWND hWnd, int x, int y, int w, int h, LPCTSTR text);

#endif